﻿using System;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using IOleServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;

namespace Hotels4U.JsLibProject
{
    [Guid(GuidList.ProjectFactoryString)]
    public class JsLibProjectFactory : ProjectFactory
    {
        private readonly Package package;

        public JsLibProjectFactory(Package package)
            : base(package)
        {
            this.package = package;
        }

        protected override ProjectNode CreateProject()
        {
            var node = new JsLibProjectNode(this.package);
            node.SetSite((IOleServiceProvider)((IServiceProvider)this.package).GetService(typeof(IOleServiceProvider)));
            return node;
        }
    }
}