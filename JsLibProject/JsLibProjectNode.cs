﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace Hotels4U.JsLibProject
{
    public class JsLibProjectNode : ProjectNode
    {
        private static readonly ImageList ImageList;
        private static int imageIndex;
        private readonly Package package;

        static JsLibProjectNode()
        {
            ImageList =
                Utilities.GetImageList(
                    typeof(JsLibProjectNode).Assembly.GetManifestResourceStream(
                        "Hotels4U.JsLibProject.Resources.js16x16.png"));
        }

        public JsLibProjectNode(Package package)
        {
            this.package = package;
            imageIndex = this.ImageHandler.ImageList.Images.Count;

            foreach(Image img in ImageList.Images)
            {
                this.ImageHandler.AddImage(img);
            }
        }

        public override int ImageIndex
        {
            get { return imageIndex; }
        }

        public override Guid ProjectGuid
        {
            get { return GuidList.ProjectFactory; }
        }

        public override string ProjectType
        {
            get { return "JsLibProjectType"; }
        }

        public override MSBuildResult Build(uint vsopts, string config, IVsOutputWindowPane output, string target)
        {
            return MSBuildResult.Successful;
        }

        public override void AddFileFromTemplate(
            string source, string target)
        {
            this.FileTemplateProcessor.UntokenFile(source, target);
            this.FileTemplateProcessor.Reset();
        }
    }
}