var sharedConfig = require('./karma.conf.shared.js');

module.exports = function(config)
{
    config.set({

        basePath: sharedConfig.basePath,

        frameworks: sharedConfig.frameworks,

        files: sharedConfig.files,

        exclude: sharedConfig.exclude,

        preprocessors: sharedConfig.preprocessors,

        reporters: sharedConfig.reporters,

        coverageReporter: sharedConfig.coverageReporter,

        junitReporter: sharedConfig.junitReporter,

        htmlReporter: sharedConfig.htmlReporter,

        port: 9889,

        colors: sharedConfig.colors,

        logLevel: config.LOG_INFO,

        autoWatch: sharedConfig.autoWatch,

        browsers: sharedConfig.browsers,

        singleRun: sharedConfig.singleRun
    });
};