﻿module.exports = {
    basePath: "./Scripts",
    frameworks: ['jasmine'],
    files: [
            'jquery-2.1.1.js',
            'bootstrap.js',
            'angular.js',
            'angular-ui/*.js',
            'angular-ui-router.js',
            'angular-mocks.js',
			__dirname + '/jasmineConfig.js',
            'angular-ui-router-test-utils.js',
			'App/**/*.js',
			'Spec/**/*.js'
    ],
    preprocessors: {
        'App/**/*.js': 'coverage',
    },
    coverageReporter: {
        type: 'html',
        dir: __dirname + '/Karma/Coverage'
    },

    junitReporter: {
        outputFile: __dirname + '/Karma/Results.xml'
    },

    htmlReporter: {
        outputDir: 'Karma/Html',
        templatePath: 'jasmine_template.html'
    },
    colors: true,
    browsers: ['PhantomJS'],
    reporters: ['story', 'junit', 'coverage', 'html'],
    autoWatch: true,
    singleRun: false,
    exclude: []
};