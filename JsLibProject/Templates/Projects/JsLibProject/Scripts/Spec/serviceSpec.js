﻿describe('myService Should', function () {
    var $httpBackend, service, rootScope, deffered;
    beforeEach(module('myModule'));

    beforeEach(function () {

        inject(function ($injector, $q, $rootScope) {
            deffered = $q.defer();
            $httpBackend = $injector.get('$httpBackend');
            service = $injector.get('myService');
            rootScope = $rootScope;
        });
    });

    it('Be defined', function(){
        expect(service).toBeDefined();
    });
});