﻿<?xml version="1.0" encoding="utf-8" ?>

<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
  <metadata>
    <id>MyJsLibProject</id>
    <version>1.0.0</version>
    <authors>Hotels4U.com</authors>
    <owners>Hotels4U.com</owners>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>My Description</description>
    <tags>
      JavaScript, AngularJS, Group Longtail Platform
    </tags>
    <dependencies>
      <dependency id="AngularJS.Core"
                  version="*" />
      <dependency id="AngularJS.UI.UI-Router"
                  version="*" />
      <dependency id="AngularJS.UI.Utils"
                  version="*" />
    </dependencies>
  </metadata>
  <files>
    <file src="Scripts\App\module.js"
          target="Scripts\App\module.js" />
    <file src="Scripts\App\service.js"
          target="Scripts\App\service.js" />
  </files>
</package>