﻿
using System;

namespace Hotels4U.JsLibProject
{
    static class GuidList
    {
        public const string PkgString = "f1afd838-70b1-4336-aad9-ac69acc71e08";
        public const string CmdSetString = "052b55f2-0812-44c9-a2e1-85742bbcc132";
        public const string ProjectFactoryString = "6B45EB89-8039-45F9-BEAE-DEF274F0DA8B";

        public static readonly Guid CmdSet = new Guid(CmdSetString);
        public static readonly Guid ProjectFactory = new Guid(ProjectFactoryString);
    };
}