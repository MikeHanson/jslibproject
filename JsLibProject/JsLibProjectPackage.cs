﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Project;
using Microsoft.VisualStudio.Shell;

namespace Hotels4U.JsLibProject
{
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)]
    [ProvideProjectFactory(typeof(JsLibProjectFactory), "JS Library Project",
        "JS Library Project Files (*.jslproj);*.jslproj", "jslproj", "jslproj", @"Templates\Projects\JsLibProject",
        LanguageVsTemplate = "JsLibProject")]
    [Guid(GuidList.PkgString)]
    public sealed class JsLibProjectPackage : ProjectPackage
    {
        public JsLibProjectPackage()
        {
            Debug.WriteLine("Entering constructor for: {0}", this.ToString());
        }

        public override string ProductUserContext
        {
            get { return string.Empty; }
        }

        protected override void Initialize()
        {
            Debug.WriteLine("Entering Initialize() of: {0}", this.ToString());
            base.Initialize();
            this.RegisterProjectFactory(new JsLibProjectFactory(this));
        }
    }
}